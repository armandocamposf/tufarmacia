<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    if (isset(Auth::user()->name)) {
        return redirect()->route('home');
    } else {
        return view('welcome');
    }
})->name('index');


Route::post('login', [UserController::class, 'login'])->name('login');
Route::get('logout', [UserController::class, 'logout'])->name('logout');
Route::get('home', [WelcomeController::class, 'home'])->name('home');

Route::group(['prefix' => 'usuarios'], function() {
    Route::get('administradores', [UserController::class, 'administradores'])->name('administradores');
    Route::get('farmacias', [UserController::class, 'farmacias'])->name('farmacias');
    Route::get('clientes', [UserController::class, 'clientes'])->name('clientes');
    Route::post('store', [UserController::class, 'store'])->name('usuario.store');
    Route::post('update', [UserController::class, 'update'])->name('usuario.update');
});

Route::group(['prefix' => 'medicamentos'], function () {
    Route::get('/{idUser}', [MedicamentoController::class, 'index'])->name('farmacias.medicamentos');
    Route::post('store', [MedicamentoController::class, 'store'])->name('farmacias.store');
});

Route::group(['prefix' => 'movil'], function () {
    Route::get('/', [AppController::class, 'index'])->name('indexMovil');
    Route::get('accion', [AppController::class, 'accion'])->name('accion');
    Route::get('crearCuenta', [AppController::class, 'crearCuenta'])->name('crearCuenta');
    Route::get('iniciarSesion', [AppController::class, 'iniciarSesion'])->name('iniciarSesion');
    Route::get('inicio/{pantalla}', [AppController::class, 'inicio'])->name('inicio');
    Route::get('mis-medicamentos', [AppController::class, 'medicamento'])->name('mis-medicamentos');
    Route::get('agregarMedicamento', [AppController::class, 'nuevoMeciamento'])->name('agregarMedicamento');
    Route::get('editar-direccion', [AppController::class, 'editarDireccion'])->name('editar-direccion');
    Route::get('editar-farmacia', [AppController::class, 'editarFarmacia'])->name('editar-farmacia');
});
