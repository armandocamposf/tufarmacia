<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
    integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
crossorigin="anonymous"></script>
<style>
    body {
        background-color: black;
    }

    .ubic-logo {
        width: 100%;
        text-align: center;
        color: white;
        font-weight: 900;
        font-size: 40px
    }

    .formularios
    {
        text-align: center;
        color: white;
        font-weight: 900;
        font-size: 30px
    }

    .logo {
        width: 20%;
        margin-left: 40%;
        margin-right: 40%;
        margin-top: 5%;
        font-size: 30px
    }

    .formularios {
        width: 80%;
        margin-left: 10%;
        margin-right: : 10%;
    }

    .tipo-cocumento {
        width: 30%;
        float: left
    }

    .documento {
        width: 68%;
        float: right
    }
</style>

<body>
    <div class="ubic-logo">
        <img class="logo" src="{{ asset('assets/images/logo.png') }}" alt="">
        TU FARMACIA
    </div>
    <form action="" id="crearCuenta">
        <div class="formularios">

            <img src="/uploads/perfiles/629d68ea11c1e_1654483178.jpeg" class="img-fluid img-thumbnail" alt="...">
            NOMBRE <br>
            RUC/DNI
            <button class="btn btn-success w-100 mt-3">Buscar Un Medicamento</button>
            <button class="btn btn-success w-100 mt-3">Mis Busquedas</button>
            <hr>
            <button class="btn btn-success w-100 mt-3">Editar Mis Datos</button>
            <button class="btn btn-success w-100 mt-3">Editar Mi Foto</button>


            <button class="btn btn-danger w-100 mt-3">Cerrar Sesion</button>
        </div>
    </form>

    <script></script>
</body>
