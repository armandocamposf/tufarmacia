<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
    integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
crossorigin="anonymous"></script>
<style>
    body {
        background-color: black;
    }

    .ubic-logo {
        width: 100%;
        text-align: center;
        color: white;
        font-weight: 900;
        font-size: 40px
    }

    .logo {
        width: 30%;
        margin-left: 35%;
        margin-right: 35%;
        margin-top: 20%;
        font-size: 30px
    }

    .formularios {
        width: 80%;
        margin-left: 10%;
        margin-right: : 10%;
    }

    .tipo-cocumento {
        width: 30%;
        float: left
    }

    .documento {
        width: 68%;
        float: right
    }
</style>

<body>
    <div class="ubic-logo">
        <img class="logo" src="{{ asset('assets/images/logo.png') }}" alt="">
        TU FARMACIA
    </div>
    <form action="" id="crearCuenta">
        <div class="formularios">
            <input type="text" name="nombre" placeholder="Nombre" class="form-control bg-dark text-white mt-3">
            <input type="text" name="nombre" placeholder="Laboratorio" class="form-control bg-dark text-white mt-3">
            <input type="text" name="nombre" placeholder="Cantidad" class="form-control bg-dark text-white mt-3">
            <input type="date" name="nombre" placeholder="" class="form-control bg-dark text-white mt-3">
            <input type="text" name="nombre" placeholder="Concentracion" class="form-control bg-dark text-white mt-3">
            <input type="text" name="nombre" placeholder="Dosis" class="form-control bg-dark text-white mt-3">
            <input type="text" name="nombre" placeholder="Precio" class="form-control bg-dark text-white mt-3">
            <input type="file" name="nombre" placeholder="Foto" class="form-control bg-dark text-white mt-3">
            <select name="" id="" class="form-control bg-dark text-white mt-3">
                <option value="">TIPO</option>
                <option value="">MARCA</option>
                <option value="">GENERICO</option>
            </select>

            <button class="btn btn-success w-100 mt-3">Guardar Medicamento</button>

            <a href="{{route('inicio', 'farmacia')}}" class="btn btn-success w-100 mt-3">Atras</a>
        </div>
    </form>

    <script></script>
</body>
