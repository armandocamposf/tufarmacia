<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>
    body {
        background-color: black;
    }

    .ubic-logo {
        width: 100%;
        text-align: center;
        color: white;
        font-weight: 900;
        font-size: 35px
    }

    .ubic-boton {
        margin-left: 15%;
        width: 70%;
        color: white;
        margin-top: 20%;
        text-align: center
    }

    .logo {
        width: 70%;
        margin-left: 15%;
        margin-right: 15%;
        margin-top: 20%;
        font-size: 30px
    }

</style>

<body>
    <div class="ubic-logo">
        <img class="logo" src="{{ asset('assets/images/logo.png') }}" alt="">
        TU FARMACIA
    </div>
    <div class="ubic-boton">
        <a href="{{route('crearCuenta')}}" class=" btn btn-success w-100">Crear Cuenta</a>
        <a href="{{route('iniciarSesion')}}" class=" btn btn-success w-100 mt-3">Iniciar Sesion</a>
    </div>
</body>
