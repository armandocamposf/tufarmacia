<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
    integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
crossorigin="anonymous"></script>
<style>
    body {
        background-color: black;
    }

    .ubic-logo {
        width: 100%;
        text-align: center;
        color: white;
        font-weight: 900;
        font-size: 40px
    }

    .logo {
        width: 30%;
        margin-left: 35%;
        margin-right: 35%;
        margin-top: 20%;
        font-size: 30px
    }

    .formularios {
        width: 80%;
        margin-left: 10%;
        margin-right: : 10%;
    }

    .tipo-cocumento {
        width: 30%;
        float: left
    }

    .documento {
        width: 68%;
        float: right
    }
</style>

<body>
    <div class="ubic-logo">
        <img class="logo" src="{{ asset('assets/images/logo.png') }}" alt="">
        TU FARMACIA
    </div>
    <form action="" id="crearCuenta">
        <div class="formularios">
            <input type="text" name="nombre" placeholder="Nombre y/o Razon Social"
                class="form-control bg-dark text-white">
            <input type="email" name="correo" placeholder="Correo Electronico"
                class="form-control bg-dark text-white mt-2">
            <input type="date" name="fecha" class="form-control bg-dark text-white mt-2">
            <div class="tipo-cocumento">
                <select name="tipo_documento" class="form-control mt-2 bg-black text-white" required>
                    <option value="1">DNI</option>
                    <option value="2">CE</option>
                    <option value="3">Pasaporte</option>
                </select>
            </div>
            <div class="documento">
                <input type="text" name="documento" placeholder="Documento"
                    class="form-control bg-dark text-white mt-2 mb-2">
            </div>
            <input type="text" placeholder="Contraseña" class="form-control bg-dark text-white mt-2">
            <input type="text" placeholder="Cconfirmar Contraseña" class="form-control bg-dark text-white mt-2">
            <select class="form-control mt-2 bg-black text-white" required name="tipo_cuenta">
                <option value="">TIPO CUENTA</option>
                <option value="2">Farmacia</option>
                <option value="3">Cliente</option>
            </select>
            <button class="btn btn-success w-100 mt-3">Crear Cuenta</button>
            <button class="btn btn-danger w-100 mt-3">Iniciar Con Google</button>
            <button class="btn btn-primary w-100 mt-3">Iniciar Con Facebook</button>

            <a href="{{route('indexMovil')}}" class="btn btn-success w-100 mt-3">Atras</a>
        </div>
    </form>

    <script></script>
</body>
