 <div class="deznav">
     <div class="deznav-scroll">

         @if (Auth::user()->rol_id == 1)
             <ul class="metismenu" id="menu">
                 <li><a href="{{ route('index') }}" class="ai-icon" aria-expanded="false">
                         <i class="flaticon-381-home"></i>
                         <span class="nav-text">Inicio</span>
                     </a>
                 </li>


                 <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                         <i class="flaticon-381-user"></i>

                         <span class="nav-text">Usuarios</span>
                     </a>
                     <ul aria-expanded="false">
                         <li><a href="{{ route('administradores') }}">Administradores</a></li>
                         <li><a href="{{ route('farmacias') }}">Farmacias</a></li>
                         <li><a href="{{ route('clientes') }}">Clientes</a></li>
                     </ul>
                 </li>


             </ul>
         @endif


     </div>
 </div>
