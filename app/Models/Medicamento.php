<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUser',
        'nombre',
        'laboratorio',
        'cantidad',
        'fecha',
        'concentracion',
        'tipo',
        'foto',
        'precio',
        'dosis'
    ];
}
