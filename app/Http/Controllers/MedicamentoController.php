<?php

namespace App\Http\Controllers;

use App\Models\{Medicamento, User};
use Illuminate\Http\Request;

class MedicamentoController extends Controller
{

    public function index($idUser)
    {
        $medicamentos = Medicamento::where('idUser', $idUser)->get();
        $user = User::find($idUser);

        return view('farmacias.medicamentos', compact('medicamentos', 'user'));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $medicamento = Medicamento::create([
            'idUser' => $request->idUser,
            'nombre' => $request->nombre,
            'laboratorio' => $request->laboratorio,
            'cantidad' => $request->cantidad,
            'fecha' => $request->fecha,
            'concentracion' => $request->concentracion,
            'dosis' => $request->dosis,
            'tipo' => $request->rol_id,
            'foto' => $this->upload_global($request->file('photo'), 'medicamentos'),
            'precio' => $request->precio
        ]);

        return back()->with('success', 'Medicamento creado con nexito');
    }


    public function show()
    {
        //
    }


    public function edit()
    {
        //
    }


    public function update()
    {
        //
    }


    public function destroy()
    {
        //
    }


    function upload_global($file, $folder)
    {

        $file_type = $file->getClientOriginalExtension();
        $folder = $folder;
        $destinationPath = public_path() . '/uploads/' . $folder;
        $destinationPathThumb = public_path() . '/uploads/' . $folder . 'thumb';
        $filename = uniqid() . '_' . time() . '.' . $file->getClientOriginalExtension();
        $url = '/uploads/' . $folder . '/' . $filename;

        if ($file->move($destinationPath . '/', $filename)) {
            return $filename;
        }
    }
}
