<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
        return view('app.index');
    }

    public function accion()
    {
        return view('app.tipo_accion');
    }

    public function crearCuenta()
    {
        return view('app.crearCuenta');
    }

    public function iniciarSesion()
    {
        return view('app.iniciarSesion');
    }

    public function inicio($pantalla)
    {
        if($pantalla == 'cliente')
        {
            return view('app.principalCliente');
        }
        else{
            return view('app.principalFarmacia');
        }
    }

    public function medicamento()
    {
        return view('app.medicamentos');
    }

    public function nuevoMeciamento()
    {
        return view('app.nuevoMedicamento');
    }

    public function editarDireccion()
    {
        return view('app.Direccion');
    }

    public function editarFarmacia()
    {
        return view('app.editarPerfil');
    }

    public function buscarMedicamento()
    {

    }
}
