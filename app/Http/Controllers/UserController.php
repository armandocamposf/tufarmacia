<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{User};

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $val = User::whereEmail($request->usuario)->wherePassword("a1Bz20ydqelm8m1wql" . md5($request->clave))->count();
        if ($val > 0) {
            $val = User::whereEmail($request->usuario)->wherePassword("a1Bz20ydqelm8m1wql" . md5($request->clave))->first();
            Auth::loginUsingId($val->id);
            return back();
        } else {
            return redirect()->route('index')->with('danger', 'Usuario o Contraseña Errada.');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('index');
    }

    public function administradores()
    {
        $usuarios = User::whereRolId(1)->get();
        $tipo = 'Administradores';

        return view('users.list', compact('usuarios', 'tipo'));
    }

    public function farmacias()
    {
        $usuarios = User::whereRolId(2)->get();
        $tipo = 'Farmacias';

        return view('users.list', compact('usuarios', 'tipo'));
    }

    public function clientes()
    {
        $usuarios = User::whereRolId(3)->get();
        $tipo = 'Clientes';

        return view('users.list', compact('usuarios', 'tipo'));
    }

    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'tipo_documento' => $request->tipo_documento,
            'documento' => $request->documento,
            'password' => "a1Bz20ydqelm8m1wql" . md5($request->password),
            'rol_id' => $request->rol_id,
            'direccion' => $request->direccion,
        ]);

        if($request->file('photo') != null)
        {
            $user->foto = $this->upload_global($request->file('photo'), 'perfiles');
        }
        $user->save();

        return back()->with('success', 'Usuario Registrado con exito');
    }


    function upload_global($file, $folder)
    {

        $file_type = $file->getClientOriginalExtension();
        $folder = $folder;
        $destinationPath = public_path() . '/uploads/' . $folder;
        $destinationPathThumb = public_path() . '/uploads/' . $folder . 'thumb';
        $filename = uniqid() . '_' . time() . '.' . $file->getClientOriginalExtension();
        $url = '/uploads/' . $folder . '/' . $filename;

        if ($file->move($destinationPath . '/', $filename)) {
            return $filename;
        }
    }
}
